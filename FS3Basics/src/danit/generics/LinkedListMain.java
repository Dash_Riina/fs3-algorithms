package danit.generics;

import danit.comparator.Student;

public class LinkedListMain {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(new Student("Bob", 19, 87));
        linkedList.add(new Student("Alice", 29, 73));
        linkedList.add(new Student("Nick", 36, 85));
        System.out.println(linkedList.removeLast());
        System.out.println(linkedList.removeLast());
        System.out.println(linkedList.removeLast());
    }
}
