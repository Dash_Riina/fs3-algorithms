package danit.generics;

import java.util.Deque;
import java.util.Scanner;

public class Unix {

        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            String path = in.nextLine();
            StringBuilder dir = new StringBuilder();
            Deque<String> dirs = new LinkedList<>();

            for(int i = 0; i < path.length(); i++);{

                char c = path.charAt(i); //i-th char in the input

                if (c == '/') {
                    addDirToStack(dir, dirs); // add prev dir name
                } else {
                    dir.append(c); // add char to current dir name
                }
            }

            if (dir.length() > 0) { //do we have last dir w/o ending '/'
                addDirToStack(dir, dirs); // add last dir name
            }
            if (dirs.isEmpty()) { // corner case "/"
                System.out.print("/");
            }

            while (!dirs.isEmpty()) { // iteration through simplified path
                System.out.print("/" + dirs.removeLast());
            }
        }

        private static void addDirToStack(StringBuilder dir, Deque<String> dirs) {
            String currentDir = dir.toString();

            if (currentDir.equals("..")) {

                if (!dirs.isEmpty()) {
                    dirs.pop();
                }
            } else if (currentDir.equals(".") || currentDir.equals("")) {

            } else {
                dirs.push(currentDir);
            }
            dir.setLength(0);
        }

    }
