package danit.generics;


public class LinkedList<E> {
    private Node first;
    private Node last;
    private int size;

    public void add(E element) {
        Node node = new Node(element);
        if (first == null) {
            first = last = node;
        } else {
            last.next = node;
            node.prev = last;
            last = node;
        }
        size++;
    }

    public E getLast() {
        return last.value;
    }

    public E removeLast() {
        Node temp = last;
        if (first == last) {
            first = last = null;
        } else {
           last.prev.next = null;
           last = last.prev;
    }
        size--;
        return temp.value;
    }

    class Node {
        Node prev;
        Node next;
        E value;

        public Node(E value) {
            this.value = value;
        }
    }
}
