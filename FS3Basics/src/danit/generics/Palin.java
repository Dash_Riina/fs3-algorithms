package danit.generics;

import java.util.Scanner;

public class Palin {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            String word = in.next();
        }
        public static Boolean isPalindrome(String string) {
            String reversedString = reverseString(string); // get reversed string
            Boolean result = string.equals(reversedString); // compare strings, save our result
            return result; // return our result
        }

    }

}
