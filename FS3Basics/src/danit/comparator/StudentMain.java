package danit.comparator;

import java.util.Arrays;

public class StudentMain {
    public static void main(String[] args) {
        Student[] students = new Student[4];
        students[0] = new Student("Bob", 19, 87);
        students[1] = new Student("Alice", 29, 73);
        students[2] = new Student("Nick", 36, 85);
        students[3] = new Student("Max", 50, 72);

        System.out.println("Students array before sorting");
        for (Student student : students) {
            System.out.println(student);
        }

        Arrays.sort(students, new GradeComparator());

        System.out.println("\nStudents array after sorting by grade");
        for (Student student : students) {
            System.out.println(student);
        }

        Arrays.sort(students, new AgeComparator());

            System.out.println("\nStudents array after sorting by grade");
            for (Student student : students) {
                System.out.println(student);
            }
                Arrays.sort(students, new NameComparator());
                System.out.println("\nStudents array after sorting by name");
                for (Student student : students) {
                    System.out.println(student);
        }
    }
}

