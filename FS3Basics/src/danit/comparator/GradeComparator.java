package danit.comparator;

import java.util.Comparator;

public class GradeComparator implements Comparator<Student> {

    @Override
    public int compare(Student student, Student student2) {
        return student.getGrade() - student2.getGrade();
    }
}
