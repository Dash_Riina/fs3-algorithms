package danit.comparator;

import java.util.Comparator;

    public class AgeComparator implements Comparator<Student> {

        @Override
        public int compare(Student student, Student student2) {
            return student2.getAge() - student.getAge();
        }
    }
