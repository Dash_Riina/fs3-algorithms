setTimeout(function() {
    updateTeamPoints('Dnipro', 120);
}, 1000);

setTimeout(function() {
    addTeam('Dynamo', 120);
}, 2000);

//setTimeout(function() {
//    deleteTeam('Shakhtar', 120);
//}, 3000);

var xhr = new XMLHttpRequest();
xhr.open('GET', '/champ', true);
xhr.onload = function() {
    var data = JSON.parse(xhr.responseText);
    console.log(data, xhr);
    getList(data);
}
xhr.send();

function getList(data) {
    var list = document.getElementById('challenge');
    list.innerHTML = '';
    data.forEach(function(fc) {
        var li = document.createElement('li');
        li.textContent = fc.name + " " + fc.points;
        list.appendChild(li);
    });
}

function updateTeamPoints(name, points) {
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', 'champ/' + name + '/' + points, true);
    xhr.onload = function() {
        console.log(xhr);
        var data = JSON.parse(xhr.responseText);
        getList(data);
    }
    xhr.send();
}

function addTeam(name, points) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'champ/' + name + '/' + points, true);
    xhr.onload = function() {
        console.log(xhr);
        var data = JSON.parse(xhr.responseText);
        if(xhr.status == 400) {
            alert(data);
        } else {
            getList(data);
        }

    }
    xhr.send();
}

function deleteTeam(name, points) {
    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', 'champ/' + name + '/' + points, true);
    xhr.onload = function() {
        console.log(xhr);
        var data = JSON.parse(xhr.responseText);
        getList(data);
    }
    xhr.send();
}