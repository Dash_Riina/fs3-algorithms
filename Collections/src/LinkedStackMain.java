public class LinkedStackMain {


    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.add(1);
        linkedStack.add(2);
        linkedStack.add(3);
        System.out.println(linkedStack.poll());
        System.out.println(linkedStack.poll());
        System.out.println(linkedStack.poll());
    }

}
