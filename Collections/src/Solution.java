import java.util.Scanner;


public class Solution {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            String str = in.nextLine();

            int x = 0;
            int y = 0;

            for(int i = 0; i < str.length(); i++){
                if(str.charAt(i) == 'w')
                    y++;
                if(str.charAt(i) == 's')
                    y--;
                else if(str.charAt(i) == 'd')
                    x++;
                else if(str.charAt(i) == 'a')
                    x--;
            }


            System.out.println(x + " " + y);
        }
    }
