
public class LinkedQueue {
    private Node head;
    private Node tail;
    private int size;

    public LinkedQueue() {
        head = null;
        tail = null;
    }

    public void add(Integer element) {
        Node newNode = new Node(element);
        if (tail == null) {
            head = tail = newNode;
        } else {
            tail.next = newNode;
            newNode.prev = tail;
            tail = newNode;
        }
        size++;
    }

    public Integer poll() {
        Integer element = head.value;
        if (size == 1) {
            head.prev = null;
            head.next = null;
        } else {
            head = head.next;
            head.prev = null;
        }
        size--;
        return element;
    }

    public int size() {
        return size;
    }


    private class Node {
        Node next;
        Node prev;
        Integer value;

        public Node(Integer value) {
            this.value = value;
        }
    }
}
