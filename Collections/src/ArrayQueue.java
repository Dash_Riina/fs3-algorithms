public class ArrayQueue {
    private Integer [] data;
    private int head;
    private int tail;
    private int size;


    public ArrayQueue(int size) {
        data = new Integer[size];
        head = 0;
        tail = 0;
    }

    public void add(Integer element) {
        if (tail < data.length) {
            data[tail] = element;
            tail++;
            size++;
        }
    }

    public Integer poll() {
        Integer element = data[head];
        data[head] = null;
        head++;
        return element;
    }

    public int size() {
        return size;
    }
}
