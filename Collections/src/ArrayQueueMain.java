public class ArrayQueueMain {

    public static void main (String[] args) {
        ArrayQueue arrayQueue = new ArrayQueue(  5);
        System.out.println("Queue size before element addition: " + arrayQueue.size());
        arrayQueue.add(5);
        System.out.println("Queue size after element deletion: " + arrayQueue.size());
        System.out.println("Polling first element: " + arrayQueue.poll());
        System.out.println("Queue size after element deletion: " + arrayQueue.size());
    }
}