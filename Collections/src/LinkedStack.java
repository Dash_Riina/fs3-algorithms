public class LinkedStack {
    private Node head;
    private Node tail;
    private int size;

    public LinkedStack() {
        head = null;
        tail = null;
    }
    public void add(Integer element) {
        // создаем новый узел со значением element
        Node newNode = new Node(element);

        if (tail == null) {
            // если стек пустой, голова и хвост ссылаются на созданный елемент
            head = tail = newNode;
        } else {
            //если стек не пустой
            tail.next = newNode;
            newNode.prev = tail;
            //сдвигаем указатель на хвостб чтобы он указывал на новый элемнт
           tail = tail.next;
        }
        size++;
    }

    public Integer poll() {
        //сохраняем значение последнего элемента (хвоста)
        Integer value = tail.value;
        if (size ==1) {
            // если элемент единственный, голова и хвост начинают ссылаться на null
            head = tail = null;
        } else {
            //если в стеке больше одного элемента
            tail = tail.prev;
            tail.next = null;
        }
        size--;
        return value;
    }

    public int size() {
        return size;
    }


    private class Node {
        Node next;
        Node prev;
        Integer value;

        public Node(Integer value) {
            this.value = value;
        }
    }

}
