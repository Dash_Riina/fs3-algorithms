public class LinkedQueueMain {
    public static void main(String[] args) {
        LinkedQueue linkedQueue = new LinkedQueue();
        System.out.println("Queue size before add: " + linkedQueue.size());
        linkedQueue.add(1);
        linkedQueue.add(2);
        linkedQueue.add(3);
        linkedQueue.add(4);
        System.out.println("Queue size after add: " + linkedQueue.size());
        System.out.println(linkedQueue.poll());
        System.out.println(linkedQueue.poll());
        System.out.println(linkedQueue.poll());
        System.out.println(linkedQueue.poll());


    }
}
