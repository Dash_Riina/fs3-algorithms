package com.danit.basic.queue;

import org.junit.Test;
import org.junit.Assert;

import java.util.Scanner;

public class ArrayQueueTest {

    @Test
    public void shouldOfferAndPollQueue() {
        ArrayQueue queue = new ArrayQueue();
        int value = 123;
        queue.offer(value);
        Assert.assertEquals(value, queue.poll());
    }

    @Test(expected = RuntimeException.class)
    public void shouldBeEmpty() {
        ArrayQueue queue = new ArrayQueue();
        queue.poll();

    }

    public static void main(String[] args) {


      //  int n = 5;
        //int k = 2;
         //int[] array = {1, 2, 3, 4, 1};

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();

        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = in.nextInt();
        }

        int sum = 0;
        for(int i = 0; i < k; i++){
             sum+= array[i];
        }

        int maxSum = sum;

         for (int i = k; i < array.length; i++) {
            int prevElement = array[i - k];

              sum -= prevElement;
              int currentElement = array[i];
              sum += currentElement;
              if ( sum > maxSum){
                  maxSum = sum;

              }
        }
        System.out.println(maxSum);


    }

}
