public class TwoDigits {
    public static String recursion(int a, int b) {
        //основна умова задачі
        if (a>b) {
            //базовий випадок
            if (a == b) {
                return Integer.toString(a);
            }
            // вводимо рекурсивну умову
            return a + " " + recursion(a - 1, b);
        } else {
            //базовий випадок
            if (a == b) {
                return Integer.toString(a);
            }
            // крок рекурсії
            return a + " " + recursion(a + 1, b);
        }
        }
    public static void main(String[] args) {
        System.out.println(recursion(20, 15));// виклик рекурсивної функції
        System.out.println(recursion(10, 15)); // виклик рекурсивної функції
    }
}
