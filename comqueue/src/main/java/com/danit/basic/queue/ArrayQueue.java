package com.danit.basic.queue;

public class ArrayQueue {

    private int capacity;
    private int[] values;
    private int size = 0;
    private int tail = 0;
    private int start = 0;


    public ArrayQueue() {
        this(10);
    }

    public ArrayQueue(int capacity) {
        this.capacity = capacity;
        this.values = new int[capacity];
    }

    public void offer(int v) {
        this.values[tail++ % capacity] = v;
        this.size++;
    }

    public int poll() {
        if (this.size == 0) {
            throw new RuntimeException("Queue is empty.");
        }

        return values[start++ % capacity];
    }
}

