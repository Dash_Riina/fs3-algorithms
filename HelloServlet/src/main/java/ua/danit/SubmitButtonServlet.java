package ua.danit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class SubmitButtonServlet extends HttpServlet {

  List<String> todo = new ArrayList<>();

  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, java.io.IOException {
    PrintWriter out = resp.getWriter();

    out.write("<html><body>");
    out.write("<form action='/*' method='POST'>");
    out.write("<input name='todoText' type='text'>");
    out.write("<button type='submit'>Submit</button>");
    out.write("</form>");

    for (String item : todo) {
      out.write("<li>");
      out.write(item);
      out.write("</li>");
    }

    out.write("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    todo.add(req.getParameter("todoText"));
    resp.sendRedirect("/*");
  }
}