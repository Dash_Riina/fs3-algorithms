package ua.danit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class ToDoApp {

  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);
    ServletContextHandler handler = new ServletContextHandler();
    ServletHolder holder = new ServletHolder(new SubmitButtonServlet());
    handler.addServlet(holder, "/*");
    server.setHandler(handler);
    server.start();
    server.join();
  }

  //  public class HelloServlet extends HttpServlet {
  //protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
  //   resp.getWriter().write("Hello servlet!");
  // }
  // }
}
